import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }
  public get() {
    return this.http.get<any>('https://sim800lvn.firebaseio.com/.json');
  }
}
