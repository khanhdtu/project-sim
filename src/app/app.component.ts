import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public dataConvert: any[];
  constructor(private appService: AppService) {
    this.dataConvert = [];
    this.appService.get()
    .toPromise()
    .then(data => {
      Object.keys(data).forEach(e => {
        const sim = {
          id: e,
          value: data[e][Object.keys(data[e])[Object.keys(data[e]).length - 1]]
        };
        this.dataConvert.push(sim);
      });
    });
  }
}
